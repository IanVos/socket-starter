
/***
 * @author Ian Vos
 * @date 2021-08-13
 * Default socket class
 * Used to define the default properties of the sockets
 * 
 */

 const { Buffer } = require('buffer');

/***
 * @class DefaultSocket
 * A default class to extend socket classes with
 * Define available encodings and default functions
 */
class DefaultSocket {
    
    // All available encodings
    encodings = ['utf8', 'utf16le', 'latin1', 'base64', 'base64url', 'hex', 'ascii', 'binary', 'ucs2'];
    
    // Constructor code
    constructor (ipAddress, port, encoding) {

        this.ipAddress = ipAddress;
        this.globalPort = port;

        if(this.encodings.includes(encoding)){
            this.encoding = encoding;
        } else {
            this.encoding = 'utf8';
        }

    }

    mainWindow; // The current active window, used to send data back
    
    globalPort; // The global port used for the socket
    ipAddress;  // The Ip adress for this socket

    encoding;   // The encoding that is used

    socket;     // The socket, ether Server or Client

    clientList = {}; // A list with all the clients

    startSocket() {}

    sendTo (target, message) {}

    sendToAll (message) {}

    endSocket() {}

    addClient(address, port, object) {
        this.clientList[JSON.stringify([address, port])] = object;
        this.mainWindow.send('clients_updated', Object.entries(this.clientList));
    }

    removeClient(address, port) {
        delete this.clientList[JSON.stringify([address, port])];
        this.mainWindow.send('clients_updated', Object.entries(this.clientList));
    }


    getClients () {
        return this.clientList;
    }

    getClient(address, port){
        return this.clientList[JSON.stringify([address, port])];
    }

    /***
     * @function msgToBuffer
     * Convert a message to a encoded buffer
     * 
     */
    msgToBuffer (msg) {
        /* Encode message and safe to buffer */
        console.log(this.encoding)

        if (Buffer.isEncoding(this.encoding)){
            const buff = Buffer.from(new String(msg), this.encoding);
            return buff;
        }
        return 1;
    }

    /***
     * @function decodeMsg
     * Decode a message to a JS string
     */
    decodeMsg (msg) {
        console.log(typeof msg)
        console.log(msg);
        msg = msg.toString(this.encoding);
        console.log(msg);
        return msg;

    }

    close() {
        if(this.socket == null){
            console.log('Trying to close socket that does not exist!');
            return 1;
        }
        // Disconnect all connections
        console.log('Disconnecting all connections');
        try{
            this.socket.disconnect();
        } catch (ERR_SOCKET_DGRAM_NOT_RUNNING){
            console.log('No connections, cannot disconnect')
        }
        this.socket.close(() => {
            this.mainWindow.send('success', 'The socket end request has been send');
        });
    }

    /** EVENT HANDLERS */

    
    /***
     * @function message_handler handle new incomming messages
     * @param msg the message
     * @param rinfo extra event information
     * 
     */
    message_handler(msg, rinfo) {
        if(rinfo){
            console.log('New message: ' + msg + '\nFrom: ' + rinfo.address + ':' + rinfo.port);
            // TODO: Check if client already exists or needs to be added
            this.addClient(rinfo.address, rinfo.port);
        }
        


        /* Decode the data and send to front end */
        let stringified = this.decodeMsg(msg);
        this.mainWindow.send('logMessage', stringified); // Send over IPC to user
    }

    /***
     * @function error_handler handle errors from a socket
     * @param err the error information
     * Must be binded with this context!
     */
    error_handler(err) {
        // TODO Send more information to frontend
        console.log(`Socket error:\n${err.stack}`);
        this.close();
        this.mainWindow.send('createSocket', 'fail');
    }
    bind_handler() {
        this.socket.setBroadcast(true);
    }
    /***
     * 
     * 
     */
    listen_handler() {
        // Socket bound
        let address = this.socket.address();
        console.log(`server listening on: ${address.address}:${address.port}`);
        this.mainWindow.send('sendMessage', 'success');
    }
    /***
     * Called when the socket is fully closed
     */
    closed_handler() {

    }
    
}

module.exports = DefaultSocket;
/***
 * updated code for udp client
 * Uses Default socket for universal operations
 * 
 * ! Same as server
 * 
 * @date 01-08-2022
 * @author Ian Vos
 * 
 */

 const { ipcMain, webContents } = require('electron');
 const dgram = require('dgram');
 const DefaultSocket = require('./defaultSocket');
 
 let udpClient = null;
 
 /***
  * @class UDPClientSocket
  * The server code for the udp socket
  * Extends the default socket code
  * 
  */
 class UDPClientSocket extends DefaultSocket {
 
     constructor(ipAddress, port, encoding) {
 
         super(ipAddress, port, encoding);
 
     }
 
     startSocket() {
 
         this.socket = dgram.createSocket('udp4'); // Create UDP server
 
         /* Set events with this bound */
         this.socket.on('error', this.error_handler.bind(this));
         this.socket.on('message', this.message_handler.bind(this));
         this.socket.bind(this.globalPort, this.bind_handler.bind(this));
         this.socket.on('listening', this.listen_handler.bind(this));
     }



 }
 
 // TODO: Define these functions in the class
 module.exports = {
     create: function(ipaddress, port, encoding){
        // Exit if socket already exists
         if(udpClient != null){
             return 1;
         }
 
         /* Create and start the socket */
         udpClient = new UDPClientSocket(ipaddress, port, encoding);
         udpClient.mainWindow = webContents.getFocusedWebContents();
         udpClient.startSocket();
         
         /* Report back that socket is created */
         console.log('Setting up server on port: ' + port);
         udpClient.mainWindow.send('log', 'UDP server has been created on port: ' + port);
     },
 
     send: function(data, numType){
        console.log(udpClient)
         // Check if data is present and the socket is created
         if(data && udpClient){
             var message = data;
             data = udpClient.msgToBuffer(message);
             udpClient.socket.send(data, udpClient.globalPort);
             udpClient.mainWindow.send('success', 'The message is on its way');
         } else {
            udpClient.mainWindow.send('error', 'Could not send, data or udpServer incorrect');
         }
     },
 
     destoy: function(){
        udpClient.endSocket();
     },
 
     endSocket: function(){
        udpClient.close();
        udpClient = null;
     }
 }
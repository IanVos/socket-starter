/***
 * updated code for TCP Server
 * Uses Default socket for universal operations
 * @date 01-08-2022
 * @author Ian Vos
 * 
 */

 const { ipcMain, webContents } = require('electron');
 const net = require('net');
 const DefaultSocket = require('./defaultSocket');
 const { timeStamp } = require('console');
  
  let tcpClient = null;
  
  /***
   * @class TCPServerSocket
   * The server code for the tcp socket
   * Extends the default socket code
   * 
   */
  class TCPClientSocket extends DefaultSocket {
  
      constructor(ipAddress, port, encoding) {
  
          super(ipAddress, port, encoding);
  
      }
  
      startSocket() {
            this.socket = new net.Socket() // Create new server instance
            /* Set events with this bound */
            this.socket.on('error', this.error_handler.bind(this));   // Called when an error has occured
            this.socket.on('close', this.closed_handler.bind(this));  // Called when socket is closed
            this.socket.on('end', this.closed_handler.bind(this));    // Called when ending is requested by server
            this.socket.on('data', this.message_handler.bind(this));  // Called when a new message is received

            this.socket.connect({port: this.globalPort, host: this.ipAddress}, () => {
                // TODO message frontend for successfull creation
                console.log('Connected succesfully');
            });
      }

      close(){
        if(this.socket == null){
            console.log('Trying to close socket that does not exist!');
            return 1;
        }
        this.socket.destroy(() => {
            this.mainWindow.send('success', 'The socket end request has been send');
        });
      }
 
     
 
  }
   
  // TODO: Define these functions in the class
  module.exports = {
      create: function(ipaddress, port, encoding){

          if(tcpClient != null){
              return 1;
          }
          /* Create and start the socket */
          tcpClient = new TCPClientSocket(ipaddress, port, encoding);
          tcpClient.mainWindow = webContents.getFocusedWebContents();
          tcpClient.startSocket();
          
          /* Report back that socket is created */
          tcpClient.mainWindow.send('log', 'TCP Client has been created on port: ' + port);
      
      },
  
      send: function(data, numType){
          // Check if data is present and the socket is created
          if(data != null && tcpClient != null){
              var message = data;
              data = tcpClient.msgToBuffer(message);
              tcpClient.socket.write(data, tcpClient.globalPort);
              tcpClient.mainWindow.send('success', 'The message is on its way');
          } else {
            tcpClient.mainWindow.send('error', 'Could not send, data or udpServer incorrect');
          }
      },
  
      destoy: function(){
        tcpClient.endSocket();
      },
  
      endSocket: function(){
         tcpClient.close();
         tcpClient = null;
      }
  }
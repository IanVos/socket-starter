/***
 * updated code for udp server
 * Uses Default socket for universal operations
 * @date 01-08-2022
 * @author Ian Vos
 * 
 */

const { ipcMain, webContents } = require('electron');
const dgram = require('dgram');
const DefaultSocket = require('./defaultSocket');

let udpServer = null;

/***
 * @class UDPServerSocket
 * The server code for the udp socket
 * Extends the default socket code
 * 
 */
class UDPServerSocket extends DefaultSocket {

    constructor(ipAddress, port, encoding) {

        super(ipAddress, port, encoding);

    }

    startSocket() {

        this.socket = dgram.createSocket('udp4'); // Create UDP server

        /* Set events with this bound */
        this.socket.on('error', this.error_handler.bind(this));
        this.socket.on('message', this.message_handler.bind(this));
        this.socket.bind(this.globalPort, this.bind_handler.bind(this));
        this.socket.on('listening', this.listen_handler.bind(this));
    }
}

// TODO: Define these functions in the class
module.exports = {
    create: function(ipaddress, port, encoding){
        if(udpServer != null){
            return 1;
        }

        /* Create and start the socket */
        udpServer = new UDPServerSocket(ipaddress, port, encoding);
        udpServer.mainWindow = webContents.getFocusedWebContents();
        udpServer.startSocket();
        
        /* Report back that socket is created */
        console.log('Setting up server on port: ' + port);
        udpServer.mainWindow.send('log', 'UDP server has been created on port: ' + port);
    },

    send: function(data, numType){
        console.log(udpServer)
        // Check if data is present and the socket is created
        if(data && udpServer){
            var message = data;
            data = udpServer.msgToBuffer(message);
            udpServer.socket.send(data, udpServer.globalPort);
            udpServer.mainWindow.send('success', 'The message is on its way');
        } else {
            udpServer.mainWindow.send('error', 'Could not send, data or udpServer incorrect');
        }
    },

    destoy: function(){
        udpServer.endSocket();
    },

    endSocket: function(){
        udpServer.close();
        udpServer = null;
    }
}
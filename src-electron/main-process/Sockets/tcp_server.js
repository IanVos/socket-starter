/***
 * updated code for TCP Server
 * Uses Default socket for universal operations
 * @date 01-08-2022
 * @author Ian Vos
 * 
 */

const { ipcMain, webContents } = require('electron');
const net = require('net');
const DefaultSocket = require('./defaultSocket');
const { timeStamp } = require('console');
 
let tcpServer = null;
 
 /***
  * @class TCPServerSocket
  * The server code for the tcp socket
  * Extends the default socket code
  * 
  */
 class TCPServerSocket extends DefaultSocket {
 
     constructor(ipAddress, port, encoding) {
 
         super(ipAddress, port, encoding);
 
     }
 
     startSocket() {
 
         this.socket = new net.Server() // Create new server instance
 
         /* Set events with this bound */
         this.socket.on('error', this.error_handler.bind(this));
         this.socket.on('listening', this.listen_handler.bind(this));
         this.socket.on('close', this.closed_handler.bind(this));

         /**
          * Start the server 
          */
         this.socket.listen(this.globalPort, this.ipAddress, () => {
            console.log('server listening on %j', this.socket.address());
            this.mainWindow.send('log', 'Server listening on: ' + this.socket.address());
    
            // TODO change socket success
            this.mainWindow.send('createSocket', 'success');
        });


        this.socket.on('connection', this.new_connection.bind(this));
     }

    /**
     * Handler for new connections
     */
     new_connection(client) {
        console.log('New connection')

        let remoteAddress = client.remoteAddress + ':' + client.remotePort;
        this.addClient(client.remoteAddress, client.port, client);
        console.log('new client connected: %s', remoteAddress);

        client.on('data', (data) => {
            let stringified = this.decodeMsg(data);
            console.log('%s Says: %s', remoteAddress, data);
            this.mainWindow.send('logMessage', stringified);
        });

        client.on('close', () => {
            console.log('connection from %s closed', remoteAddress);
        });

        client.on('error', (err) => {
            console.log('Connection %s error: %s', remoteAddress, err.message);
            mainWindow.send('createSocket', 'fail')
        });

     }

     destroy_connections(){

        let clients = this.getClients();
        for(let id in clients){
            clients[id].destroy();
        }

     }

     sendToAll(msg) {
        let buff = this.msgToBuffer(msg);
        let clients = this.getClients();
        for(let id in clients){
            clients[id].write(buff);
        }
     }

     sendTo(target, msg) {
        let buff = this.msgToBuffer(msg);
        let target_socket = this.getClient(target, this.globalPort);
        target_socket.write(buff);
     }

 }

 // TODO: Define these functions in the class
 module.exports = {
     create: function(ipaddress, port, encoding){
         if(tcpServer != null){
             return 1;
         }
 
         /* Create and start the socket */
         tcpServer = new TCPServerSocket(ipaddress, port, encoding);
         tcpServer.mainWindow = webContents.getFocusedWebContents();
         tcpServer.startSocket();
         
         /* Report back that socket is created */
         console.log('Setting up server on port: ' + port);
         tcpServer.mainWindow.send('log', 'UDP server has been created on port: ' + port);
     
     },
 
     send: function(data, numType){
 
         // Check if data is present and the socket is created
         if(data != null && tcpServer != null){
             tcpServer.sendToAll(data);
             tcpServer.mainWindow.send('success', 'The message is on its way');
         } else {
            tcpServer.mainWindow.send('error', 'Could not send, data or udpServer incorrect');
         }
     },
 
     destoy: function(){
        tcpServer.endSocket();
     },
 
     endSocket: function(){
        tcpServer.destroy_connections()
        tcpServer.close();
        tcpServer = null;
     }
 }
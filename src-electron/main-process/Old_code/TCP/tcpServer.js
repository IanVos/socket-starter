
/***
 * @author Ian Vos
 * @date 2020-07-11
 * tcp server code
 * Create, maintain and use a tcp socket as server
 */

const { ipcMain, webContents } = require('electron')
const net = require('net');
const { endSocket } = require('./tcpClient');

var tcpServer;

/* 
 * Main Window, for cumunication to frontend 
 * Gets set by webContents.getFocusedWebContents() every time a ICP message is received
 */
let mainWindow;

var Server = function(ipAddress, port){
    var self = this;

    /* Create a new server */
    self.nets = new net.Server()
    self.clients = [];
    
    self.nets.listen(port, ipAddress, function(){
        
        console.log('server listening on %j', self.nets.address());
        mainWindow.send('log', 'Server listening on: ' + self.nets.address());

        // TODO change socket success
        mainWindow.send('createSocket', 'success');
    });

    /**
     * ON: connection
     * When a connection is made, init and save
     */
    self.nets.on('connection',function(sock){
        /* Log the client's address and port */
        let remoteAddress = sock.remoteAddress + ':' + sock.remotePort;
        console.log('new client connected: %s', remoteAddress);
        mainWindow.send('log', remoteAddress);

        /* Add the new client to the client list */
        self.clients.push(sock);

        /* Create data event */
        sock.on('data', function(data) {
            
            console.log('%s Says: %s', remoteAddress, data);
            /* Decode the data to a normal string */
            let stringified = new TextDecoder().decode(data);
            mainWindow.send('logMessage', stringified);
        });

        sock.on('close',  function () {
            console.log('connection from %s closed', remoteAddress);
        });

        sock.on('error', function (err) {
            console.log('Connection %s error: %s', remoteAddress, err.message);

            // TODO impove fail protocol
            mainWindow.send('createSocket', 'fail')
        });
    })


}

module.exports = {

    create: function(ipadress, port){
        if(tcpServer){
            return 'Alread set'
        }
        mainWindow = mainWindow = webContents.getFocusedWebContents();
        tcpServer = new Server(ipadress, port);
        console.log('Setting up server on port: ' + port);
        mainWindow.send('log', 'Tcp server seems to be created on port: ' + port);
    },

    send: function(data){
        if(data && tcpServer){

            // TODO Check if client is alive and sending is posible
            tcpServer.clients.forEach(client => {
                client.write(data);
            });

            mainWindow.send('success', 'The message is on its way');
        } else {
            mainWindow.send('error', 'Could not send, data or tcpServer incorrect');
        }
    },
    destoy: function(){
        this.endSocket();
    },
    endSocket: function(){
        /* End the server */
        console.log('TCP Server ending');
        if(tcpServer){
            tcpServer.nets.close();
            tcpServer.clients.forEach(client => {
                client.end();
            });
            tcpServer = null;

            if(!tcpServer){
                mainWindow.send('success', 'The socket end request has been send');
            } else{
                mainWindow.send('error', 'TCP tcpServer could not be completly ended');
            }
        } else{
            mainWindow.send('error', 'Looks like there is no TCP Server');
        }
    }
}
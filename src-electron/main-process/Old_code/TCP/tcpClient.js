
/***
 * @author Ian Vos
 * @date 2020-07-11
 * tcp client code
 * Create, maintain and use a tcp socket as client
 */

const { ipcMain, webContents } = require('electron')
const net = require('net');

var tcpClient;

/* 
 * Main Window, for cumunication to frontend 
 * Gets set by webContents.getFocusedWebContents() every time a ICP message is received
 */
let mainWindow;

var Client = function(ipadress, port){

    /* Create a new socket and set encoding */
    this.nets = net.Socket();
    this.nets.setEncoding('utf8');

    this.nets.connect({port: port, host: ipadress}, function(){
        console.log('Connected succesfully')
    })
    this.nets.on('data', function(data) {
        console.log('New message: ' + data);
        mainWindow.send('logMessage', data);
    });
    
    this.nets.on('end', function() {
        console.log('Requested an end to the TCP connection');
        mainWindow.send('createSocket', 'fail')
    });

    this.nets.on('error', function(err) {
        console.log('Error occured');
        mainWindow.send('createSocket', 'fail')
        mainWindow.send('logMessage', err)
    });
};

module.exports = {
    create: function(ipadress, port){
        if(tcpClient){
            return 'already set'
        }
        mainWindow = mainWindow = webContents.getFocusedWebContents();
        tcpClient = new Client(ipadress, port);
        mainWindow.send('log', 'Tcp seems to be created');
    },
    send: function(data){
        if(data && tcpClient){
            tcpClient.nets.write(data, 'utf8');
            mainWindow.send('success', 'The message is on its way');
        } else {
            mainWindow.send('error', 'Could not send, data or tcpClient incorrect');
        }
    },
    destoy: function(){
        /* Stop the connection */
        if(tcpClient){
            tcpClient.nets.destoy();
            tcpClient = null;
            
            mainWindow.send('success', 'The socket should now be destoryed');
        } else{
            mainWindow.send('error', 'Looks like there is no TCP client');
        }
    },
    endSocket: function(){
        /* Ask server for end */
        console.log('TCP client ending');
        if(tcpClient){
            tcpClient.nets.end();
            tcpClient = null;
            if(!tcpClient){
                mainWindow.send('success', 'The socket end request has been send');
            } else{
                mainWindow.send('error', 'TCP client could not be completly ended');
            }
        } else{
            mainWindow.send('error', 'Looks like there is no TCP client');
        }
    }


}
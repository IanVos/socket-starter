
/***
 * @author Ian Vos
 * @date 2020-07-11 -
 * ! OLD CODE 
 * udp server code
 * Create, maintain and use a udp datagram as Server
 */

const { ipcMain, webContents } = require('electron');
const dgram = require('dgram');

let mainWindow;
let udpServer;
let portGlobal;

var Server = function(ipaddress, port){
    self = this;
    portGlobal = port;
    self.nets = dgram.createSocket('udp4');

    

    self.nets.on('error', (err) => {
        console.log(`server error:\n${err.stack}`);
        self.nets.close();
        mainWindow.send('createSocket', 'fail')
    });

    self.nets.on('message', (msg, rinfo) => {
        console.log('New message: ' + msg) + '\nFrom: ' + rinfo.address + ':' + rinfo.port;

        /* Decode the data */
        let stringified = new TextDecoder().decode(msg);
        mainWindow.send('logMessage', stringified);
    });

    self.nets.on('listening', () => {
        let address = self.nets.address();
        self.nets.setBroadcast(true);
        console.log(`server listening ${address.address}:${address.port}`);
    });

    self.nets.bind(function(){
        self.nets.setBroadcast(true);
    });

    mainWindow.send('sendMessage', 'success')

};

module.exports = {
    create: function(ipaddress, port){
        if(udpServer){
            return 'already set';
        }
        mainWindow = mainWindow = webContents.getFocusedWebContents();
        udpServer = new Server('127.0.0.1', port);
        console.log('Setting up server on port: ' + port);
        mainWindow.send('log', 'UDP server has been created on port: ' + port);
    },

    /**
     * Send data to all clients
     * @param {*} data 
     * @param {*} numType 
     */
    send: function(data, numType){

        if(data && udpServer){


            var message = data;

            /* Check how to send */
            if(numType == 1){   // If numtype is one send as Uint8Array
                var charArray = data.split('');
                console.log(charArray);
                message = Buffer.alloc(charArray.length);
                for (let index = 0; index < charArray.length; index++) {
                    message.writeUInt8(parseInt( charArray[index]), index );
                }
            }

            udpServer.nets.send(data, portGlobal);
            mainWindow.send('success', 'The message is on its way');
        } else {
            mainWindow.send('error', 'Could not send, data or udpServer incorrect');
        }
    },


    destoy: function(){
        this.endSocket();
    },
    endSocket: function(){
        if(udpServer == null){
            console.log('Trying to close socket that does not exist!');
            return 1;
        }

        // Disconnect all connections
        console.log('Disconnecting all connections');
        udpServer.nets.disconnect();

        udpServer.nets.close();
        udpServer = null;
        if(udpServer == null){
            mainWindow.send('success', 'The socket end request has been send');
        } else{
            mainWindow.send('error', 'UDP server could not be completly ended');
        }
        console.log('Ended UDP socket');
    }
}

/***
 * @author Ian Vos
 * @date 2022-07-31
 * udp Client code
 * Create, maintain and use a udp datagram as client
 */

const { ipcMain, webContents } = require('electron');
const dgram = require('dgram');

/* Global vars */
let udpClient;
let mainWindow;

var Client = function(ipaddress, port){
    self = this;

    self.nets = dgram.createSocket('udp4');

    self.nets.bind(function () {
        self.nets.setBroadcast(true);
    });

    self.nets.connect(port, ipaddress, (err) => {
        mainWindow.send('success', 'UDP created');
    });

    self.nets.on('message', function (message, remote) {

        console.log(remote.address + ':' + remote.port +' - ' + message);
        console.log('New message: ' + message) + '\nFrom: ' + remote.address + ':' + remote.port;
        mainWindow.send('logMessage', message)
    
    });

}

module.exports = {

    create: function(ipaddress, port){
        if(udpClient){
            return 'already set';
        }
        mainWindow = mainWindow = webContents.getFocusedWebContents();
        udpClient = new Client(ipaddress, port);
        mainWindow.send('log', 'UDP client seems to be created');

    },

    /***
     * Send data to target
     * Called in socket.js
     * 
     */
    send: function(data, numType){
        if(data && udpClient){ // Check if there is data and the UDP client is available

            var message = data;

            /* Check how to send */
            if(numType == 1){   // If numtype is one send as Uint8Array
                var charArray = data.split('');
                console.log(charArray);
                message = Buffer.alloc(charArray.length);
                for (let index = 0; index < charArray.length; index++) {
                    message.writeUInt8(parseInt( charArray[index]), index );
                }
            }

            /* Send the message */
            udpClient.nets.send(message, (err) => {
                console.log('UPD message sent');
            });

            /* Send success message */
            mainWindow.send('success', 'The message is on its way');
        } else {
            mainWindow.send('error', 'Could not send, data or udpClient incorrect');
        }
    },
    destoy: function(){
        this.endSocket();
    },
    endSocket: function(){
        if(udpClient == null){
            console.log('Trying to close socket that does not exist!');
            return 1;
        }

        // Disconnect all connections
        console.log('Disconnecting all connections');
        udpClient.nets.disconnect();

        udpClient.nets.close();
        udpClient = null;
        if(udpClient == null){
            mainWindow.send('success', 'The socket end request has been send');
        } else{
            mainWindow.send('error', 'UDP client could not be completly ended');
        }
        console.log('Ended UDP socket');
    }

}
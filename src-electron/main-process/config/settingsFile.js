const fs = require('fs');
const { ipcMain, webContents } = require('electron');

/* The main window instance */
let mainWindow;


/***
 * @function writeConfig
 * @param data the dataobject that needs to be saved
 * @param fileName the of the config file
 * Save a new config file
 */
function writeConfig(data, fileName){
  let config = JSON.stringify(data);

  fileName.replace('/', '').replace('.', '');
  fs.writeFile('./' + fileName + '.json', config, (err) => {
    if(err){
      console.log('There was an error while saving the config \n');
      console.log(err.message);
      return
    }
    console.log('File saved! \n');
  });
}

/***
 * @function readConfig
 * @param fileName the name of the file that needs to be read
 * Read a new config and send it to the frontend 
 */
function readConfig(fileName){
  let reaData = fs.readFileSync('./' + fileName + '.json');
  let data;

  try{
    data = JSON.parse(readData);
  } catch(err){
    console.log(err);
    mainWindow.send('error', err.message);
  }

  mainWindow.send('readConfig', data);
}

/***
 * @function readStandard
 * Read the standard config file with the basic information
 */
function readStandard(){
  readConfig('standardConfig');
}

module.exports = {
  
  /***
   * @function init
   * Initialize the settings file instance and setup event listeners
   */
  init: () => {
    mainWindow = mainWindow = webContents.getFocusedWebContents();

    /***
     * @event readConfig
     * Read the configuration
     */
    ipcMain.on('readConfig', (event, configName) => {
      mainWindow = webContents.getFocusedWebContents();
      
      /* If config name is empty read the default  */
      if(configName == null){
        readStandard();
      } else {
        readConfig(configName);
      }

  }),


  }



}
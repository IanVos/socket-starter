/***
 * @author Ian Vos
 * Socket module, with IPC
 * Different socket functionalty
 */

 /* Include Ipc, webcontents to communciate with the frondend */
const { ipcMain, webContents } = require('electron');

/* Include Net for TCP and dgram for UDP */
const dgram = require('dgram');

/*
 * Inlcude the network modules 
 */ 
const tcpClient = require('./Sockets/tcp_client');
const tcpServer = require('./Sockets/tcp_server');

const udpClient = require('./Sockets/udp_client');
const udpServer = require('./Sockets/udp_server');

/* 
 * Main Window var for communication to frontend 
 * Gets set by webContents.getFocusedWebContents() every time a ICP message is received
 */
var mainWindow;
var currentSocket;

/*** 
 * @function createSocket
 * @param ipaddress the target ip address
 * @param port the target port
 * @param protocol the protocol that needs to be used (tcp/udp)
 * @param role the role of the socket (server = 0/client = 1)
 * Set the current socket and create a new socket
 * The current socket will be used to connect and send data to the other end
 ***/
function createSocket(ipaddress, port, protocol, role, encoding){

    /** 
     * TODO: Create checks so the socket cannot change when running 
     * TODO: Kill previous socket connection before creating a new one
     */

    console.log('Creating a new socket at: ' + ipaddress + ' - ' + port + ' using ' + encoding + ' encoding\n');
    console.log('Settings: ' + protocol + ' - ' + role + '\n');
    /* Find the current socket settings and set acordingly */
    switch(protocol){
        case 'udp':
            if(role == 'client'){
                currentSocket = udpClient;
            } else if(role == 'server') {
                currentSocket = udpServer;
            }
            break;
        case 'tcp':
            if(role == 'client'){
                currentSocket = tcpClient;
            } else if(role == 'server') {
                currentSocket = tcpServer;
            }
            break;
    }
    
    /* Create a new socket */
    currentSocket.create(ipaddress, port, encoding);
}

module.exports = {
    /***
     * @function init
     * initialize the socket module
     * Setup event listeners
     */
    init: function() {

        /***
         * @event createSocket
         * @param event the event instance
         * @param arg the current settings
         * Create a new socket based on the current settings
         */
        ipcMain.on('createSocket', (event, arg) => {
            /* Set main window to current */
            mainWindow = webContents.getFocusedWebContents();

            createSocket(arg.ipaddress, arg.port, arg.socketType, arg.socketFunct, arg.encoding);
            

            event.reply('createSocket', 'done');
        }),
        
        /***
         * @event sendMessage
         * @param event the event instance
         * @param message the message that needs to be send
         * @param numType send as number (1) or text (0)
         * Send a message to the target(s)
         */
        ipcMain.on('sendMessage', (event, message, numType) => {
            mainWindow = webContents.getFocusedWebContents();
            
            currentSocket.send(message, numType);
            event.reply('sendMessage', 'success');
        }),

        /***
         * @event removeSocket
         * @param event the event instance
         * @param arg nothing
         * Remove the current socket by ending it
         */
        ipcMain.on('removeSocket', (event, arg) => {
            mainWindow = webContents.getFocusedWebContents();

            console.log('Remove socket trigger');
            currentSocket.endSocket();
           
            event.reply('removeSocket', 'killed')
        })
    }

}
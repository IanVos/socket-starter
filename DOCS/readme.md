# Read Me
This program is created to easily start new socket programs.
Because it is always anoying to write a custom socket program to receive data from your embedded device this program can be used.
With this program you can select the correct settings and connecect with your socket.

## How it works
On the front-end, locateded in `src` -> `pages` -> `Index.vue`, you can select your settings and press the create socket button. Using the `IPC` protocols the front-end communicates with the nodejs backend and can preform actions. 

When the program is started the `electron-main.js` will initialize the exports of the `socket.js` file. This will start an IPC listener that will trigger when an action is preformed on the front-end. 
When a new socket is made the settings will be transmitted using IPC and the corresponding socket classes are loaded. This is done by changing the `currentSocket` variable in `socket.js`. All the sockets share functionality and use specific functions that can be called by the `socket.js` IPC function.